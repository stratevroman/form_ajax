$(document).ready(function() {

    $('#form').popup({
        opacity: 0.3,
        transition: 'all 0.3s'
    });
    $(".page__btn").click(function() {
        $(".page__btn").prop("disabled", true);
        setTimeout(function() {
            $(".page__btn").prop("disabled", false);
            console.log('open')
            $('#form').popup('show');
        }, 1000);
    })
    //Аякс отправка форм
    //Документация: http://api.jquery.com/jquery.ajax/
    $("form").submit(function() {
        event.preventDefault(); // отменяем событие по умолчанию
        if (validateForm()) { // если есть ошибки возвращает true
            return false; // прерываем выполнение скрипта
        }
        $("form").popup('hide');
        console.log('ajax true')
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: $("form").serialize()
        }).done(function(data) {
            alert("Спасибо за заявку!");
            setTimeout(function() {
                $.fancybox.close();
            }, 1000);
        });
        return false;
    });
$("form #name,form #phone2,form #email").change(function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $('#phone2').val();
     
    if(name.length != 0 && email.length != 0 && phone.length != 0) {
        $('#submit').removeAttr('disabled');
    } else {
        $('#submit').attr('disabled', 'disabled');
    }
})
function validateForm() {
    $(".text-error").remove();
    
    // Проверка логина    
    var name    = $("#name");
    var name_validation = name.val()?false:true;
    if (name_validation) {
      name.after('<span class="text-error for-name">Поле имя обязательно к заполнению</span>');
      // $(".for-name").css({top: name.position().top + name.outerHeight() + 2});
    } 
    $("#name").toggleClass('error', name_validation );
    
    var reg     = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    var email    = $("#email");
    var email_validation = email.val()?false:true;
  
    if ( email_validation ) {
      email.after('<span class="text-error for-email">Поле e-mail обязательно к заполнению</span>');
      // $(".for-email").css({top: email.position().top + email.outerHeight() + 2});
    } else if ( !reg.test( email.val() ) ) {
      email_validation = true;
      email.after('<span class="text-error for-email">Вы указали недопустимый e-mail</span>');
      // $(".for-email").css({top: email.position().top + email.outerHeight() + 2});
    }
    $("#email").toggleClass('error', email_validation );
    
    // Проверка паролей
    
    var phone    = $("#phone2");
    
    var phone_validation = phone.val()?false:true;
    var reg_phone = /^\d[\d\(\)\ -]{4,14}\d$/;
    if (phone_validation) {
       phone.after('<span class="text-error for-phone">Поле номера обязательно к заполнению</span>');
      // $(".for-phone").css({top: phone.position().top + phone.outerHeight() + 2});
    } else if ( !reg_phone.test( phone.val() ) ) {
      phone_validation = true;
      phone.after('<span class="text-error for-phone">Вы указали недопустимый телефон</span>');
      // $(".for-phone").css({top: email.position().top + email.outerHeight() + 2});
    }
    
    $("#phone2").toggleClass('error', phone_validation );
    
    return ( name_validation  || email_validation || phone_validation);
  }

});